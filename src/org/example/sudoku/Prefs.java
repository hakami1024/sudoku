package org.example.sudoku;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;

public class Prefs extends PreferenceActivity implements OnSharedPreferenceChangeListener{

	Music music;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
		music=(Music)getIntent().getSerializableExtra("music");
	}
	
	public static boolean getMusic(Context context){
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("music", false);
	}
	
	public static boolean getHints (Context context){
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("hints", false);
	}

	@Override
	protected void onResume(){
		super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}
	
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		Log.d("in Prefs, onSharedPreferenceChanged", "sharedPreference="+sharedPreferences+" key="+key);
		if (key.equals("music")){
			Context context=getApplicationContext();
			Log.d("Prefs, onSharedPreferenceChanged", "going to change music state");
		
			if (getMusic(context)) startService(new Intent(this, Music.class));
			else stopService(new Intent(this, Music.class));
		}
	}
		
}
