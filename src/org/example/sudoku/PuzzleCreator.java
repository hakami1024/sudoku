package org.example.sudoku;

import java.util.Random;
import android.util.Log;

public class PuzzleCreator {
	
	int level;
	private int[] puzzleCell=new int[9*9], firstCell=new int[9*9];
    PuzzleCreator(int diff) {
    	level=diff;
    	if (getDifficulty()!=-1){ //!=DifficultyContine
    		String[] bCell=createCell();
    		puzzleCell=fromPuzzleString(bCell);
    		firstCell=fromPuzzleString(bCell);
    	}
        
	}
	
    private int getDifficulty(){
    	return level;
    }
	
    private String[] createCell(){
		String nums, lineNums;
		String[] cell=new String[9];
		//Log.i("PuzzleCreator, createCell()", "it started");
		for (int i=0; i<9; i++) cell[i]="000000000";
		Random r=new Random();
	    int randPos;
	    char ch, chPrec;
		for (int i=0; i<9; i++){
			nums="123456789";
			for (int j=0; j<9; j++){
				lineNums=nums;
				ch='0';
				do{
					randPos=r.nextInt(lineNums.length());
					chPrec=lineNums.charAt(randPos);
					if (!(isInSquare(j, i, chPrec, cell) || isInRow(j, chPrec, cell))){
						ch=chPrec;
					}
					lineNums=lineNums.replaceFirst(""+chPrec, "");
					//Log.e(" the num: ", ""+chPrec+" (ch="+ch+") inSquare: "+isInSquare(j, i, chPrec, cell)+" inRow: "+isInRow(j, i, chPrec, cell));
				}
				while((ch=='0') && (lineNums.length()>0));
				if(ch=='0'){
					cell[i]="000000000";
					nums="123456789";
					j=-1;
					//Log.e(" #rebuild", "rebuild");
				}
				else{
					cell[i]=cell[i].replaceFirst("0", ""+ch);
					//Log.e(" #"+j+" / "+i, cell[i]);
					nums=nums.replaceFirst(""+ch, "");
				}
			}
		}
		//for (int i=0; i<9; i++){
		//	Log.e(" �"+i, cell[i]);
		//}
		cell=deleteSomeNums(cell);
		//for (int i=0; i<9; i++){
		//	Log.e(" �"+i, cell[i]);
		//}
		//Log.e("Game", "Cell created");
		return cell;
	}
	
	private boolean isInSquare(int x, int y, char ch, String[] cell){
		int minX=(x/3)*3;
		int minY=(y/3)*3;
		for (int i=minY; i<minY+3; i++){
			for (int j=minX; j<minX+3; j++){
				if (cell[i].charAt(j)==ch) return true;
			}
		}
		return false;
	}
	
    private boolean isInRow(int x, char ch, String[] cell){
		for(int i=0; i<9; i++){
			if (cell[i].charAt(x)==ch){
				return true;
			}
		}
		return false;
	}
    
    private String[] deleteSomeNums(String[] cell){
		Random r=new Random();
		int randPos;
		int randPosY;
		for(int i=0; i<9; i++){
			randPos=r.nextInt(9);
			cell[randPos]=cell[randPos].replaceFirst(""+cell[randPos].charAt(i), "0");
		}
		for (int i=0; i<9; i++)
			if (cell[i].indexOf("0")==-1) 
				cell[i]=cell[i].replaceFirst(""+cell[i].charAt(r.nextInt(9)), "0");
		for (int i=0; i<9; i+=3){
			for (int j=0; j<9; j+=3){
				randPos=0;
				for (int y=i; y<i+3; y++){
					for (int x=j; x<j+3; x++){
						if (cell[y].charAt(x)!='0') randPos++;
					}
				}
				randPosY=0;
				if (randPos==9) {
					do randPos=r.nextInt(j+3);
					while(randPos<j);
					do randPosY=r.nextInt(i+3);
					while(randPosY<i);
					cell[randPosY].replaceFirst(""+cell[randPosY].charAt(randPos), "0");
				}
			}
		}
		
		String tiles;
		for (int i=0; i<9; i+=3){
			for(int j=0; j<9; j+=3){
				for(int yi=0; yi<3; yi++){
					tiles=""+cell[i+yi].charAt(j)+cell[i+yi].charAt(j+1)+cell[i+yi].charAt(j+2);
					randPos=r.nextInt(3);
					if (((isInSquare((j+3)%9, i+yi, tiles.charAt(randPos), cell))
							&& (isInSquare((j+6)%9, i+yi, tiles.charAt(randPos), cell)))
							||
					   ((isInSquare(j, (i+3)%9, tiles.charAt(randPos), cell))
							   && (isInSquare(j, (i+6)%9, tiles.charAt(randPos), cell)))
							)
						cell[i+yi]=cell[i+yi].replaceFirst(""+cell[i+yi].charAt(j+randPos), "0");
				}
			}
		}
		
		///*
		  boolean canDelete;
		  char ch;
		  for (int i=0; i<9; i++){	
			tiles="123456789";	
			while(tiles.length()>0){
				ch=tiles.charAt(r.nextInt(tiles.length()));
				if (cell[i].indexOf(""+ch)!=-1){
					canDelete=true;
					for (int j=0; j<9; j++){
						if (!isInRow(j, ch, cell)) canDelete=false;
						}
					if (canDelete) cell[i]=cell[i].replaceFirst(""+ch, "0");
				}
				tiles=tiles.replaceFirst(""+ch, "");
			}
		}
		  for (int i=0; i<9; i++){
			  tiles="123456789";
			  while (tiles.length()>0){
				  ch=tiles.charAt(r.nextInt(tiles.length()));
				  if (isInRow(i, ch, cell)){
					  canDelete=true;
					  for (int j=0; j<9; j++){
						  if (cell[j].indexOf(""+ch)==-1)canDelete=false;
					  }
					  if (canDelete) cell[i]=cell[i].replaceFirst(""+ch, "0");
				  }
				  tiles=tiles.replaceFirst(""+ch, "");
			  }
		  }
		  
		if (getDifficulty()==Game.DIFFICULTY_HARD) {
			Log.i("in PuzzleCreator, deleteSomeNums", "difficulty is hard");
			tiles=String.valueOf(r.nextInt(9)+1);
			for (int i=0; i<9; i++)
				cell[i]=cell[i].replaceAll(tiles, "0");
		}
		else Log.i("in PuzzleCreator, deleteSomeNums", "difficulty="+getDifficulty());
		//*/
		return cell;
	}

    protected int[] getPuzzle(){
    	return puzzleCell;
    }
    
    protected void setFirstPuzzle(int[] puz){
    	firstCell=puz;
    }
    
    protected void setPuzzleCell(int[] puz){
    	puzzleCell=puz;
    }
    
    public int[] getFirstPuzzle(){
    	return firstCell;
    }
   
    public final void printPuzzle(){
    	String s;
    	for (int i=0; i<9; i++){
    		s="";
    		for (int j=0; j<9; j++){
    			s=s+puzzleCell[i*9+j];
    		}
    		Log.i("PuzzleCreator, getPuzzle()", " puzzleCell from line "+i+" : "+s);
    	}
    	
    }
    
    protected final int[] fromPuzzleString(String cell){
    	int [] c=new int[9*9];
    	for (int i=0; i<9; i++){
    		for (int j=0; j<9; j++){
    			c[i*9+j]=cell.charAt(i*9+j)-(int)'0';
    		}
    	}
    	return c;
    }
    
    protected final int[] fromPuzzleString(String[] cell){
		int[] puz=new int[9*9];
		for (int i=0; i<9; i++){
			for (int j=0; j<9; j++){
				puz[i*9+j]=(int)(cell[i].charAt(j))-(int)('0');
			}
		}
		return puz;
	}
	
    protected final String toPuzzleString(String[] bCell){
    	String sPuzzle="";
    	for (String i: bCell){
    		sPuzzle+=i;
    	}
    	return sPuzzle;
    }

    protected final String toPuzzleString(int[] iCell){
    	String sPuzzle="";
    	for(int i=0; i<9; i++){
    		for (int j=0; j<9; j++){
    			sPuzzle+=String.valueOf(iCell[i*9+j]);
    		}
    	}
    	return sPuzzle;
    }
}
