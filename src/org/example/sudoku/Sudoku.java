package org.example.sudoku;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Menu;
import android.view.MenuInflater;

public class Sudoku extends Activity implements OnClickListener{
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        View continueButton=findViewById(R.id.continue_button);
        continueButton.setOnClickListener(this);
        View newGameButton=findViewById(R.id.new_game_button);
        newGameButton.setOnClickListener(this);
        View settingsButton=findViewById(R.id.settings_button);
        settingsButton.setOnClickListener(this);
        View aboutButton=findViewById(R.id.about_button);
        aboutButton.setOnClickListener(this);
        View exitButton=findViewById(R.id.exit_button);
        exitButton.setOnClickListener(this);
        Log.d("Sudoku, onCreate", "going to enable music");
        if (Prefs.getMusic(getApplicationContext())) startService(new Intent(this, Music.class));
        else Log.d("Sudoku, onCreate", "Music is not enabled");
       // Music.play(this, R.raw.music00);
    }
    
    @Override
	protected void onResume(){
		super.onResume();
	//	Music.play(this, R.raw.music00);
	}
	
	@Override
	protected void onPause(){
		super.onPause();
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
		Log.d("onDestroy in Sudoku class", "It started");
		//Music.stop(this);

	}	
	
    public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.continue_button:
			Log.d("in Sudoku, onClick", "continue_button");
			startGame(-1); //- DifficultyContine
			break;
		case R.id.new_game_button:
			openNewGameDialog();
			break;
		case R.id.settings_button:
			startActivity(new Intent(this, Prefs.class));
			break;
		case R.id.about_button:
			startActivity(new Intent(this, About.class));
			break;
		case R.id.exit_button:
			finish();
			break;
		}
	}
	
	private void openNewGameDialog(){
		new AlertDialog.Builder(this)
		    .setTitle(R.string.new_game_label)
		    .setItems(R.array.difficulty, new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int i) {
					startGame(i);					
				}
			}).show();
	}
   
	public void startGame(int i){
    	Log.d("in Sudoku, startGame", "procedure started");
		Intent intent=new Intent(Sudoku.this, Game.class);
		intent.putExtra(Game.KEY_DIFFICULTY, i);
		Log.d("in Sudoku, startGame", "starting Activity, difficulty="+i);
		startActivity(intent);
		intent=null;
	}
	
    @Override
	public boolean onCreateOptionsMenu(Menu menu){
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater=getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}
		
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.settings:
			Log.w("menu will be started", "menu will be started");
			startActivity(new Intent(this, Prefs.class));
			Log.w("menu is started", "menu is started");
			return true;
		default: Log.e("Sudoku, onOptionsItemSelected", "isn't settings, id="+item.getItemId()+" id.settings="+R.id.settings);
		}
		return false;
		}
}
