package org.example.sudoku;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class About extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
	}
	protected void onResume(){
		super.onResume();
	}
	
	protected void onDestroy(){
		super.onDestroy();
	   // stopService(new Intent(this, Music.class));
	}

}
