package org.example.sudoku;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
//import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

public class Keypad extends Dialog{
	
	private final View keys[]=new View[9];
	//private final int useds[];
	private final PuzzleView puzzleView;
	
	public Keypad(Context context, /*int useds[],*/ PuzzleView puzzleView){
		super(context);
		//Log.i("Keypad", "Keypad builder started");
	//	this.useds=useds;
		this.puzzleView=puzzleView;
		//Log.i("Keypad", "Keypad builder finished");
	}
	
	@Override 
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		//Log.i("Keypad", "onCreate begins");
		setTitle(R.string.keypad_label);
		setContentView(R.layout.keypad);
		findViews();
		//Log.i("Keypad", "Elements are setting invisible");
		//for (int element:useds){
		//	if (element!=0)
		//		keys[element-1].setVisibility(View.INVISIBLE);
		//}
		setListeners();
		//Log.i("Keypad", "onCreate finished");
	}
	
	private void findViews() {
		keys[0] = findViewById(R.id.keypad_1);
		keys[1] = findViewById(R.id.keypad_2);
		keys[2] = findViewById(R.id.keypad_3);
		keys[3] = findViewById(R.id.keypad_4);
		keys[4] = findViewById(R.id.keypad_5);
		keys[5] = findViewById(R.id.keypad_6);
		keys[6] = findViewById(R.id.keypad_7);
		keys[7] = findViewById(R.id.keypad_8);
		keys[8] = findViewById(R.id.keypad_9);
		}
	
    private void setListeners(){
		for (int i=0; i<keys.length; i++){
			final int t=i;
			keys[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					returnResult(t);
				}});
		}
	}
	
//	private boolean isValid(int tile){
//		for (int t : useds)
//			if (t==tile) return false;
//		return true;
//	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
		/*int tile=0;
		if (keyCode>KeyEvent.KEYCODE_0 && keyCode<=KeyEvent.KEYCODE_9)
			tile=keyCode-KeyEvent.KEYCODE_0;
		else */if(keyCode!=KeyEvent.KEYCODE_0 && keyCode!=KeyEvent.KEYCODE_SPACE)
			return super.onKeyDown(keyCode, event);
	//	if (isValid(tile))
	//		returnResult(tile);
		return true;
	}
	
	private void returnResult(int tile){
		puzzleView.setSelectedTile(tile+1);
		dismiss();
		
		
	}

}
