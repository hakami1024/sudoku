package org.example.sudoku;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

public class PuzzleView extends View{
	private final Game game;

	public PuzzleView (Context context) {
		super(context);
		this.game=(Game)context;
		setFocusable(true);
		setFocusableInTouchMode(true);
	}
	
	private float width;
	private float height;
	private int selX;
	private int selY;
	private final Rect selRect=new Rect(),
	                   r=new Rect();
	private Paint paint1=new Paint(),
			      paint2=new Paint(),
			      foreground=new Paint(Paint.ANTI_ALIAS_FLAG);
	     
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh){
		width=w / 9f;
		height=h / 9f;
		getRect(selX, selY, selRect);
		super.onSizeChanged(w, h, oldw, oldh);
	}
	
	private void getRect(int x, int y, Rect rect){
		rect.set((int)(x*width), (int)(y*height), (int)(x*width+width), (int)(y*height+height));
	}
	
	@Override
	protected void onDraw(Canvas canvas){
		Log.i("PuzzleView", "onDraw activated");
		paint1.setColor(getResources().getColor(R.color.puzzle_background));
		canvas.drawRect(0, 0, getWidth(), getHeight(), paint1);
		
		paint1.setColor(getResources().getColor(R.color.puzzle_hilite));
		paint2.setColor(getResources().getColor(R.color.puzzle_light));
		
		for (int i = 0; i < 9; i++) {
			canvas.drawLine(0, i * height, getWidth(), i * height,
			paint2);
			canvas.drawLine(0, i * height + 1, getWidth(), i * height
			+ 1, paint1);
			canvas.drawLine(i * width, 0, i * width, getHeight(),
			paint2);
			canvas.drawLine(i * width + 1, 0, i * width + 1,
			getHeight(), paint1);
			}
		
		int i=0;
		paint2.setColor(getResources().getColor(R.color.puzzle_dark));
		
		while(i<9){
			canvas.drawLine(0, i*height, getWidth(), i*height, paint2);
			canvas.drawLine(0, i*height+1, getWidth(), i*height+1, paint1);
			canvas.drawLine(i*width, 0, i*width, getHeight(), paint2);
			canvas.drawLine(i*width+1, 0, i*width+1, getHeight(), paint1);
			i=i+3;
		}
		
		if (Prefs.getHints(getContext())){
			int c[]={getResources().getColor(R.color.puzzle_hint_0),
					getResources().getColor(R.color.puzzle_hint_1),
					getResources().getColor(R.color.puzzle_hint_2)};
			game.pc.printPuzzle();
			for (i=0; i<9; i++){
				for(int j=0; j<9; j++){
					if(this.game.pc.getPuzzle()[i*9+j]==0){
						//Log.i("PuzzleView, onDraw", ""+i+", "+j);
						int movesLeft=9-game.getUsedTiles(j, i).length;
						if (movesLeft<c.length){
						getRect(j, i, r);
						paint1.setColor(c[movesLeft]);
						canvas.drawRect(r, paint1);}
					}
				}
			}
		}
		
		foreground.setColor(getResources().getColor(R.color.puzzle_foreground));
		foreground.setStyle(Style.FILL);
		foreground.setTextSize(height*0.75f);
		foreground.setTextScaleX(width/height);
		foreground.setTextAlign(Paint.Align.CENTER);
		
		//Log.i("onDraw", "onDraw is near the text");
		FontMetrics fm=foreground.getFontMetrics();
		float x=width/2;
		float y=height/2-(fm.ascent+fm.descent)/2;
		String theText;
		for (i=0; i<9; i++){
			for (int j=0; j<9; j++){
				theText=this.game.getTileString(i, j);
				canvas.drawText(theText, i*width+x, j*height+y, foreground);
				//Log.i("The text", " "+theText);
			}
		}
		paint1.setColor(getResources().getColor(R.color.puzzle_selected));
		canvas.drawRect(selRect, paint1);
		
		//Log.i("PuzzleView", "onDraw finished");

	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
		if ((keyCode>=KeyEvent.KEYCODE_0) && (keyCode<=KeyEvent.KEYCODE_9)) 
			setSelectedTile(keyCode-KeyEvent.KEYCODE_0);
		else if (keyCode==KeyEvent.KEYCODE_SPACE)
			setSelectedTile(0);
		else switch(keyCode){
		case KeyEvent.KEYCODE_DPAD_DOWN:
			select(selX, selY+1);
			break;
		case KeyEvent.KEYCODE_DPAD_UP:
			select(selX, selY-1);
			break;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			select(selX+1, selY);
			break;
		case KeyEvent.KEYCODE_DPAD_LEFT:
			select(selX-1, selY);
			break;
	    default:
	    	return super.onKeyDown(keyCode, event);
		}
		return true;
	}
	
	@Override
    public boolean onTouchEvent(MotionEvent event){
		if (event.getAction()!=MotionEvent.ACTION_DOWN)
			return super.onTouchEvent(event);
		select((int)(event.getX()/width), (int)(event.getY()/height));
		game.showKeypadOrError(selX, selY);
		return true;
	}

	private void select(int x, int y){
		invalidate(selRect);
		selX=Math.min(Math.max(x, 0), 8);
		selY=Math.min(Math.max(y, 0), 8);
		getRect(selX, selY, selRect);
		invalidate(selRect);
	}
	
	public void setSelectedTile(int tile){
		if(game.setTileIfValid(selX,selY,tile))
			invalidate();
	}
}