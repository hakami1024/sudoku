package org.example.sudoku;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

public class Game extends Activity
{
	static final String KEY_DIFFICULTY="org.example.sudoku.difficulty";
	protected final String PREF_PUZZLE = "puzzle";
	protected final String PREF_FIRST_PUZZLE="first puzzle";
	static final int DIFFICULTY_EASY=0;
	//static final int DIFFICULTY_MEDIUM=1;
	static final int DIFFICULTY_HARD=1;
	protected final int DIFFICULTY_CONTINUE=-1;
    int[] puzzle=new int [9*9];
	PuzzleCreator pc;
	private PuzzleView puzzleView;
	public int diff;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		diff=getIntent().getIntExtra(KEY_DIFFICULTY, DIFFICULTY_EASY);
		Log.d("Game, onCreate", "Came to music");
		pc=new PuzzleCreator(diff);
		if (diff==DIFFICULTY_CONTINUE){ 
			Log.i("Game, onCreate","diff==continue settings, firstPuzzle changing");
			try{
			pc.setFirstPuzzle(pc.fromPuzzleString(getPreferences(MODE_PRIVATE).getString(PREF_FIRST_PUZZLE, pc.toPuzzleString(pc.getFirstPuzzle()))));
			Log.i("Game, onCreate","diff==continue settings, puzzle changing");
			pc.setPuzzleCell(pc.fromPuzzleString(getPreferences(MODE_PRIVATE).getString(PREF_PUZZLE, pc.toPuzzleString(puzzle))));
			}
			catch( Exception ex )
			{
				Toast.makeText( this, "No game to continue", Toast.LENGTH_LONG ).show();
				onDestroy();
			}
		}
		puzzle=pc.getPuzzle();
		calculateUsedTiles();
		
		puzzleView=new PuzzleView(this);
		setContentView(puzzleView);
		puzzleView.requestFocus();
		getIntent().putExtra(KEY_DIFFICULTY, DIFFICULTY_CONTINUE);
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		getPreferences(MODE_PRIVATE).edit().putString(PREF_PUZZLE, pc.toPuzzleString(puzzle)).commit();
		getPreferences(MODE_PRIVATE).edit().putString(PREF_FIRST_PUZZLE, pc.toPuzzleString(pc.getFirstPuzzle())).commit();
		this.finish();
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		Log.d("onDestroy in Game class", "It started");
		getPreferences(MODE_PRIVATE).edit().putString(PREF_PUZZLE, pc.toPuzzleString(puzzle)).commit();
		getPreferences(MODE_PRIVATE).edit().putString(PREF_FIRST_PUZZLE, pc.toPuzzleString(pc.getFirstPuzzle())).commit();
	    pc=null;
	    stopService(new Intent(this, Music.class));
	}

    protected void showKeypadOrError(int x, int y)
    {
    	int[] puzzleAtBeginning=pc.getFirstPuzzle();
    	//Log.i("Game, showKeypadOrError", "puzzleAtBeginning["+y+"*9, "+x+"]="+puzzleAtBeginning[y*9+x]);
	if (puzzleAtBeginning[y*9+x]==0)
	{
		int[] tiles=getUsedTiles(x,y);
		//for (int i=0; i<tiles.length; i++){
		//	Log.i(" Tile #"+i, " "+tiles[i]);
		//}
		if (tiles.length==9)
		{
			Toast toast= Toast.makeText(this, R.string.no_moves_label, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		}
		else
		{
		//	Log.i("Try make dialog", "in showKeypadOrError");
			Dialog v=new Keypad(this, /*tiles,*/puzzleView);
			v.show();
		}
		}
	//else Log.i("Game. ShowKeypadOrError", "puzzleAtBeginning="+puzzleAtBeginning[y*9+x]);
	}
	
	protected boolean setTileIfValid(int x, int y, int value){
		
		/*int tiles[]=getUsedTiles(x, y);
		if (value!=0){
			for (int tile:tiles){
				if (tile==value){
					return false;
				}
			}
		}*/
		
	//	pc.printPuzzle();
		
		//Log.i("in Game, setTileIfValid", "going to setTile");
		puzzle[y*9+x]=value;
		
		//pc.printPuzzle();
		
		calculateUsedTiles();
		return true;
	}
	
	private final int used[][][]=new int[9][9][];
	
	protected int[] getUsedTiles(int x, int y)
	{
		return used[x][y];
	}
		
	private void calculateUsedTiles()
	{
		for (int i=0; i<9; i++)
		{
			for (int j=0; j<9; j++)
			{
				used[i][j]=calculateUsedTiles(i, j);
			}
		}
	//	Log.i("in calculateUsedTiles", "tiles calculated");
	}
	
	private int[] calculateUsedTiles(int x, int y)
	{
		int[] c=new int[9];
		int t;
		for(int i=0; i<9; i++)
		{
			if (i==y) continue;
			t=puzzle[i*9+x];
			if (t!=0) c[t-1]=t;
		}
		for (int i=0; i<9; i++)
		{
			if(i==x) continue;
			t=puzzle[y*9+i];
			if (t!=0) c[t-1]=t;
		}
		
		int startX=(x/3)*3;
		int startY=(y/3)*3;
		for(int j=startY; j<startY+3; j++){
		    for(int i=startX; i<startX+3; i++)
		    {
				if (i==x && j==y) continue;
				t=puzzle[j*9+i];
				if (t!=0) c[t-1]=t;
		    }
		}
		
		int nUsed=0;
		for (int t1:c){
			if (t1!=0) nUsed++;
		}
		int[] c1=new int[nUsed];
		nUsed=0;
		for (int t1:c){
			if (t1!=0) c1[nUsed++]=t1;
		}
		
		return c1;
	}

	protected String getTileString(int x, int y){
		if (puzzle[y*9+x]==0) return "";
		else return String.valueOf(puzzle[y*9+x]);
	}

}
